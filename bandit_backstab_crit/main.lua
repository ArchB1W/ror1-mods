local bandit = Survivor.find("Bandit")

callback.register("preHit", function(damager, reciever)
    local damagerParent = damager:getParent()

    if
        type(damagerParent) ~= "PlayerInstance"
        or damagerParent:getSurvivor() ~= bandit
    then
        return
    end

    -- https://saturnyoshi.gitlab.io/RoRML-Docs/misc/variables.html#general
    local damagerAccessor = damager:getAccessor()
    if
        damagerAccessor.critical == 0
        -- This might be weird with projectiles such as Dynamite because you could technically turn around to get a free crit
        and damagerParent:getFacingDirection() == reciever:getFacingDirection()
    then
        damagerAccessor.critical = 1
        -- Double these as per the RoRML Docs
        damagerAccessor.damage = damagerAccessor.damage * 2
        damagerAccessor.damage_fake = damagerAccessor.damage_fake * 2
    end
end, 1000) -- Very high priority because crits should be one of the first things that apply
