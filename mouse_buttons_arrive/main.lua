-- Blacklists for characters that are buggy and/or don't work
local primaryBlacklist = {}
local secondaryBlacklist = {}

-- Add characters to blacklist
callback.register("postLoad", function()
    -- Vanilla
    local sniper = Survivor.find("Sniper")
    secondaryBlacklist[sniper] = true

    -- https://rainfusion.net/mod/878e509c-da50-4cd5-a427-b788fe8a550b
    -- Mouse Sniper already implements mouse buttons itself, let's do this to be safe
    if modloader.checkMod("msnipe") then
        local mouseSniper = Survivor.find("Mouse Sniper")
        primaryBlacklist[mouseSniper] = true
        secondaryBlacklist[mouseSniper] = true
    end
end)

callback.register("onPlayerStep", function(player)
    local isLocalPlayer = net.localPlayer == player
    local offlineAndNotController = not net.online
        and input.getPlayerGamepad(player) == nil

    if not (isLocalPlayer or offlineAndNotController) then
        return
    end

    local queuedSkills = {}

    local survivor = player:getSurvivor()

    if not primaryBlacklist[survivor] and input.checkMouse("left") == input.HELD then
        table.insert(queuedSkills, "z")
    end

    if not secondaryBlacklist[survivor] and input.checkMouse("right") == input.HELD then
        table.insert(queuedSkills, "x")
    end

    for _, skillLetter in ipairs(queuedSkills) do
        -- https://saturnyoshi.gitlab.io/RoRML-Docs/misc/variables_actor.html#movement-and-skills
        player:set(skillLetter .. "_skill", 1)
    end
end)
