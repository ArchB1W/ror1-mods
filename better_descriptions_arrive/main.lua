callback.register("postLoad", function()
    local namespaces = modloader.getMods()
    table.insert(namespaces, "vanilla")

    for _, namespace in ipairs(namespaces) do
        for _, item in ipairs(Item.findAll(namespace)) do
            local log = item:getLog()
            if log ~= nil then
                -- https://saturnyoshi.gitlab.io/RoRML-Docs/misc/coloredTextFormatting.html
                item.pickupText = log.description:gsub("&.&", ""):gsub("&..&", "")
            end
        end
    end
end)
