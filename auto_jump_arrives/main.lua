callback.register("onPlayerStep", function(player)
    -- Make sure we're on the ground to avoid triggering things like Hopoo Feathers
    if player:get("moveUpHold") == 1 and player:get("free") == 0 then
        player:set("moveUp", 1)
    end
end)
