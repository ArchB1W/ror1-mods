local artifacts = { "antigrav" }
for _, artifact in ipairs(artifacts) do
    require("artifacts." .. artifact .. ".main")
end
