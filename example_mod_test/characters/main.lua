local characters = { "jam" }
for _, character in ipairs(characters) do
    require("characters." .. character .. ".main")
end
