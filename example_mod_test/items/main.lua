local items = {
    common = { "juice" },
    uncommon = { "scarf" },
    legendary = {},
    equipment = { "voltberry" },
    boss = {},
    special = {},
}

for tier, tieredItems in pairs(items) do
    for _, item in ipairs(tieredItems) do
        require("items." .. tier .. "." .. item .. ".main")
    end
end
