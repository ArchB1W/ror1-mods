MouseAimArrives = {}

-- Kudos to https://github.com/BurningWitness/gamemaker-risk-of-rain for letting me find stuff I normally wouldn't have

local commando = Survivor.find("Commando")
local enforcer = Survivor.find("Enforcer")
local bandit = Survivor.find("Bandit")
local huntress = Survivor.find("Huntress")
local hand = Survivor.find("HAN-D")
local engineer = Survivor.find("Engineer")
local miner = Survivor.find("Miner")
local sniper = Survivor.find("Sniper")
local acrid = Survivor.find("Acrid")
local mercenary = Survivor.find("Mercenary")
local loader = Survivor.find("Loader")
local chef = Survivor.find("CHEF")

-- These types control when the mouse's position should affect the player's direction
MouseAimArrives.types = {
    always = 1,
    useMoveDirection = 2,
    never = 3,
}
-- The configuration here is meant to emulate similar behavior to Risk of Rain Returns
-- I believe they're as follows:
-- |> attack (except those with movement) (all): always
-- |> backdash or grapple (Sniper, Miner, & Loader): always
-- |> forward movement (attack or not) (Commando, Huntress, & Chef): useMoveDirection
-- |> player's orientation doesn't matter: never
local t = MouseAimArrives.types
MouseAimArrives.configuration = {
    [commando] = { t.always, t.always, t.useMoveDirection, t.always },
    [enforcer] = { t.always, t.always, t.always, t.always },
    [bandit] = { t.always, t.always, t.never, t.always },
    -- Doesn't work with Huntress's Tertiary [1]
    [huntress] = { t.always, t.always, t.useMoveDirection, t.always },
    [hand] = { t.always, t.never, t.never, t.always },
    [engineer] = { t.always, t.never, t.never, t.never },
    [miner] = { t.always, t.always, t.always, t.always },
    -- Doesn't work properly with Sniper's Secondary [2]
    [sniper] = { t.always, t.always, t.always, t.never },
    [acrid] = { t.always, t.always, t.never, t.always },
    [mercenary] = { t.always, t.always, t.useMoveDirection, t.always },
    [loader] = { t.always, t.never, t.always, t.never },
    [chef] = { t.always, t.always, t.useMoveDirection, t.never },

    -- [1]: Doesn't work at all, not quite sure why
    -- [2]: Partially works because of "onPlayerStep", but doesn't at all "useSkill"
}

-- To configure other characters, you can do something like this
callback.register("postLoad", function()
    local moddedGuys = {
        RTSchars = {
            REX = { t.always, t.never, t.always, t.always },
        },
    }
    for mod, survivors in pairs(moddedGuys) do
        if modloader.checkMod(mod) then
            for name, config in pairs(survivors) do
                local survivor = Survivor.find(name, mod)
                MouseAimArrives.configuration[survivor] = config
            end
        end
    end
end)

local function setDirection(accessor, direction)
    -- image_xscale2 seems to be required for the "useSkill" callback by Huntress
    accessor.image_xscale = direction
    accessor.image_xscale2 = direction
end

local function setDirectionFromMouse(accessor)
    local mouseX, _ = input.getMousePos()
    if mouseX > accessor.x then
        setDirection(accessor, 1)
    else
        setDirection(accessor, -1)
    end
end

callback.register("postLoad", function()
    for survivor, skills in pairs(MouseAimArrives.configuration) do
        survivor:addCallback("useSkill", function(player, skillIndex)
            local accessor = player:getAccessor()
            local skillType = skills[skillIndex]

            if
                -- See: https://saturnyoshi.gitlab.io/RoRML-Docs/misc/variables_actor.html?highlight=activity#id2
                -- Type 0: No Limitations
                -- Type 3: Can Jump
                -- Type 4: Can Walk & Jump
                not (
                    accessor.activity_type == 0
                    or accessor.activity_type == 3
                    or accessor.activity_type == 4
                ) or skillType == t.never
            then
                return
            elseif skillType == t.always then
                setDirectionFromMouse(accessor)
            elseif skillType == t.useMoveDirection then
                if accessor.moveLeft == 1 then
                    setDirection(accessor, -1)
                elseif accessor.moveRight == 1 then
                    setDirection(accessor, 1)
                else
                    setDirectionFromMouse(accessor)
                end
            end
        end)
    end
end)

callback.register("onPlayerStep", function(player)
    local accessor = player:getAccessor()

    if accessor.activity_type == 0 then
        setDirectionFromMouse(accessor)
    end
end)

export("MouseAimArrives", MouseAimArrives)
